package com.example.decodemediastream

import android.media.AudioAttributes
import android.media.AudioFormat
import android.media.AudioTrack
import android.media.MediaCodec
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.example.decodemediastream.databinding.ActivityMainBinding
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import java.net.URISyntaxException

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    private var mSocket: Socket? = null
    private lateinit var audioTrack: AudioTrack
    private val mListByteArray: ArrayList<ByteArray> by lazy { ArrayList() }
    private var isPlaying: Boolean = false

    private var audioDecoder = MediaCodec.createDecoderByType("audio/g711-mlaw")

    private var prevPresentationTimes = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = mainBinding.root
        setContentView(view)
        mSocket = try {
            val opts = IO.Options()
            opts.transports = arrayOf(WebSocket.NAME)
            IO.socket("https://twilio-stream-1102.herokuapp.com", opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
            null
        }
        mSocket?.connect()
        mSocket?.on("connect") {
            Log.d("MediaStream", "socket -> connect")
            listenMediaStream()
        }
        initView()
    }

    private fun listenMediaStream() {
        mSocket?.on("media-stream") {
            Log.d("MediaStream", "socket -> media-stream")
            runOnUiThread {
                val data = it[0] as ByteArray

                Log.d("MediaStream", "audio track -> data size -> ${data.size}")

                audioTrack.write(data, 44, data.size - 44)
//                mListByteArray.add(data)
//                playAudio()
            }
        }
    }

    private fun initView() {
        val minBufferSize = AudioTrack.getMinBufferSize(
            SAMPLE_RATE,
            ENCODING,
            CHANNEL_MASK
        )
        Log.d("MediaStream", "audiotrack -> min buffer size -> $minBufferSize")
        audioTrack =
            AudioTrack.Builder()
//                .setAudioAttributes(
//                    AudioAttributes.Builder()
//                        .setUsage(AudioAttributes.USAGE_MEDIA)
//                        .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
//                        .build()
//                )
                .setAudioFormat(
                    AudioFormat.Builder()
                        .setSampleRate(SAMPLE_RATE)
                        .setEncoding(ENCODING)
                        .setChannelMask(CHANNEL_MASK)
                        .build()
                )
                .setBufferSizeInBytes(minBufferSize)
                .setTransferMode(AudioTrack.MODE_STREAM)
                .build()

        audioTrack.play()

        Log.d("MediaStream", "audio track -> channel count -> ${audioTrack.channelCount}")
        Log.d("MediaStream", "audio track -> sample rate -> ${audioTrack.sampleRate}")
    }

    private fun playAudio() {
        if (isPlaying) {
            return
        }
        if (mListByteArray.isNotEmpty()) {
            val byteArray = mListByteArray.removeAt(0)


//            val mediaFormat =
//                MediaFormat.createAudioFormat(MediaFormat.MIMETYPE_AUDIO_G711_MLAW, 8000, 1)
//            mediaFormat.setByteBuffer("byteArray", ByteBuffer.wrap(byteArray))
//            audioDecoder.configure(mediaFormat, null, null, 0)

            Log.d("MediaStream", "media -> ${byteArray.size}")

            val audio = audioTrack.write(byteArray, 44, byteArray.size - 44)

            Handler(Looper.getMainLooper()).postDelayed({
                playAudio()
            }, 500)
            return
        }
        isPlaying = false
    }

//    fun decode(buf: ByteArray?, length: Int, audioTrack: AudioTrack) {
//        //Enter ByteBuffer
//        val codecInputBuffers: Array<ByteBuffer> = audioDecoder.getInputBuffers()
//        //Output ByteBuffer
//        val codecOutputBuffers: Array<ByteBuffer> = audioDecoder.getOutputBuffers()
//        //waiting time
//        val kTimeOutUs: Long = 1000
//        try {
//            //Return an input buffer index containing valid data, -1-> does not exist
//            val inputBufIndex: Int = audioDecoder.dequeueInputBuffer(kTimeOutUs)
//            if (inputBufIndex >= 0) {
//                //Get the current ByteBuffer
//                val currentBuf = codecInputBuffers[inputBufIndex]
//                currentBuf.clear()
//                currentBuf.put(buf, 0, length)
//                //Submit the input buffer of the specified index to the decoder
//                audioDecoder.queueInputBuffer(inputBufIndex, 0, length, getPTSUs(), 0)
//            }
//            //Codec buffer
//            val info = MediaCodec.BufferInfo()
//            var outputBufferIndex: Int = audioDecoder.dequeueOutputBuffer(info, kTimeOutUs)
//            var outputBuffer: ByteBuffer
//            while (outputBufferIndex >= 0) {
//                //Get the decoded ByteBuffer
//                outputBuffer = codecOutputBuffers[outputBufferIndex]
//                //Used to save the decoded data
//                val outData = ByteArray(info.size)
//                outputBuffer[outData]
//                //Empty the cache
//                outputBuffer.clear()
//
//                //Play the decoded data
//                audioTrack.write(outData, 0, outData.size)
//                //Release the decoded buffer
//                audioDecoder.releaseOutputBuffer(outputBufferIndex, false)
//                outputBufferIndex = audioDecoder.dequeueOutputBuffer(info, kTimeOutUs)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    private fun getPTSUs(): Long {
//        var result = System.nanoTime() / 1000
//        if (result < prevPresentationTimes) {
//            result += (prevPresentationTimes - result)
//        }
//        return result
//    }

    companion object {
        private const val HEADER_SIZE = 58
        private const val SAMPLE_RATE = 16000
        private const val ENCODING = AudioFormat.ENCODING_PCM_16BIT
        private const val CHANNEL_MASK = AudioFormat.CHANNEL_OUT_MONO
        private const val PERMISSION_REQ_ID_RECORD_AUDIO = 22
    }
}